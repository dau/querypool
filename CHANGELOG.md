# CHANGELOG.md

## (unreleased)

## 1.0.0

New features:
  - Query pool for cooperative queries
  - Query pool for non-cooperative queries
  - Query pool for cooperative queries in gevent patched environment
  - Query pool for non-cooperative queries in gevent patched environment
